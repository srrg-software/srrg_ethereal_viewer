# Srrg Ethereal Viewer

ThreeJS/webGL viewer for TXTIO dataset.

![](https://media.giphy.com/media/xT9IgyjCRxqsEROGU8/giphy.gif)

# How to use it

Open `txtio_loader.html` in a browser and load a txtio dataset that contains
odometry. Use `N` to go to next images and `R` to refresh the scene.

## Example Dataset

TUM Dataset: [fr2/desk](https://drive.google.com/file/d/0BxhfDMgREiwXb1dJS01fVjVBc2s/view?usp=sharing)

**IMPORTANT**
Due to the browser restrictions, the dataset has to be uncompressed directly in the `srrg_ethereal_viewer` folder, since
there is no access to the local path of the file.

After the uncompression, the `srrg_ethereal_viewer` folder should contain the following folders and files:

* `mpr_output.txt`, the dataset root
* `depth_js`, containing the depth images
* `rgb`, containing the rgb image
* `js`, containing the auxiliar javascript src
* `txtio_loader.html`, the html page

Open `txtio_loader.txt` and load `mpr_output.txt`. Press repeatingly `N` to load the next images.

# Authors

* [Bartolomeo Della Corte](http://www.diag.uniroma1.it/~dellacorte/)
