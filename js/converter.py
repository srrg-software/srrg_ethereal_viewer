#!/ usr / bin / python
from PIL import Image
import sys
import os
import matplotlib as plt
import numpy as np

def convertToJS(filename, new_filename):
    
    
    im = Image.open(filename)
    pixelMap = im.load()      
    max_depth = 5
    img = Image.new( 'L', im.size )
    pixelsNew = img.load()
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            new_value = (pixelMap[i,j]/5)*0.001 #convert to meters
            #print new_value
            if new_value < max_depth:
                value_scaled = np.float(new_value*255/max_depth)
#                print "(Y)   ", value_scaled
                #                print "(.)(.)", pixelsNew[i,j]
                value_scaled_converted = np.uint8(value_scaled)
#                print "(.)(.)", value_scaled_converted
                pixelsNew[i,j] = np.int(value_scaled) 
#                print "8=D   ", pixelsNew[i,j]
            else:
                pixelsNew[i,j] = 0;
    im.close()
    img.save(new_filename) 
    img.close()
    print "done with: " + new_filename

        
# def convertToJS(filename, new_filename):
#         im = Image.open(filename)
#         pixelMap = im.load()
#         max_depth = 5
#         img = Image.new( im.mode, im.size)
#         pixelsNew = img.load()
#         for i in range(img.size[0]):
#                 for j in range(img.size[1]):
#                     new_value = (pixelMap[i,j]/5)*0.001 #convert to meters
#                     #print new_value
#                     if new_value < max_depth:
#                         value_scaled = (new_value*255/max_depth)
#                         print "(Y)   ", round(value_scaled)
#                         print "(.)(.)", pixelsNew[i,j]
#                         pixelsNew[i,j] = value_scaled
#                         print "8=D   ", pixelsNew[i,j]
#                     else:
#                         pixelsNew[i,j] = 99;
#         im.close()
#         img.save(new_filename) 
#         img.close()
#         print "done with: " + new_filename


list_file=sys.argv[1]

print '\nList File: ', str(list_file)


for line in open(str(list_file)):
	content = line.split()
	depth_img = content[0]
        
        depth_img_converted = depth_img.split('png')[0]
        depth_img_converted = depth_img_converted + 'js.png'

        #convert depth image to PGM and put them in the new directory
        convertToJS(depth_img, depth_img_converted)

